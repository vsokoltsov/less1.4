FROM debian:9 as builder

ENV LUAJIT_LIB /usr/local/lib/
ENV LUAJIT_INC /usr/local/include/luajit-2.1/

RUN apt update && \
    apt install -y libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev build-essential manpages-dev libpcre++-dev vim

WORKDIR /nginx

ADD http://nginx.org/download/nginx-1.13.6.tar.gz /nginx/nginx-1.13.6.tar.gz
ADD https://openresty.org/download/openresty-1.15.8.3.tar.gz /openresty/openresty-1.15.8.3.tar.gz
ADD https://github.com/openresty/lua-resty-core/archive/v0.1.17.tar.gz /lua-resty-core/v0.1.17.tar.gz
ADD https://github.com/openresty/lua-resty-lrucache/archive/v0.09.tar.gz /lua-lruache/v0.09.tar.gz
ADD https://github.com/openresty/luajit2/archive/v2.1-20200102.tar.gz /luajit/luajit.tar.gz
ADD https://github.com/vision5/ngx_devel_kit/archive/v0.3.1.tar.gz /nginx-devel-kit/nginx-devel-kit.tar.gz
ADD https://github.com/openresty/lua-nginx-module/archive/v0.10.15.tar.gz /lua-nginx/lua-nginx.tar.gz


RUN tar xvfz /nginx/nginx-1.13.6.tar.gz
RUN tar xvfz /openresty/openresty-1.15.8.3.tar.gz
RUN tar xvfz /lua-resty-core/v0.1.17.tar.gz
RUN tar xvfz /lua-lruache/v0.09.tar.gz
RUN tar xvfz /luajit/luajit.tar.gz
RUN tar xvfz /nginx-devel-kit/nginx-devel-kit.tar.gz
RUN tar xvfz /lua-nginx/lua-nginx.tar.gz

WORKDIR /nginx/luajit2-2.1-20200102
RUN make && make install

WORKDIR /nginx/lua-resty-core-0.1.17
RUN make install

WORKDIR /nginx/lua-resty-lrucache-0.09
RUN make install

WORKDIR /nginx/openresty-1.15.8.3
RUN ./configure -j2 && make -j2 && make install

WORKDIR /nginx/nginx-1.13.6

RUN ./configure --prefix=/usr/local --with-ld-opt="-Wl,-rpath,/usr/local/lib/" --add-module=/nginx/ngx_devel_kit-0.3.1 --add-module=/nginx/lua-nginx-module-0.10.15 && \
    make -j2 && \ 
    make install

EXPOSE 80

FROM alpine:latest
WORKDIR /root/
COPY --from=builder /usr/local/sbin/nginx /usr/local/sbin/nginx
COPY --from=builder /usr/local/ /usr/local/
COPY --from=builder /lib/x86_64-linux-gnu/ /lib/x86_64-linux-gnu/
COPY --from=builder /lib64 /lib64
COPY --from=builder /lib /lib

CMD ["/usr/local/sbin/nginx", "-g", "daemon off;"]