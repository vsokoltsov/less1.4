#!/bin/bash

set -e

docker run -it -d --name nginx --rm -p 8000:80 --mount type=bind,source=$(pwd)/nginx.conf,destination=/usr/local/conf/nginx.conf vsokoltsovnginx:latest